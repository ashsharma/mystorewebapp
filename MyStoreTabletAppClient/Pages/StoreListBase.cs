﻿using MyStoreTabletAppClient.Data.Models;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Pages
{
    public class StoreListBase : ComponentBase
    {
        string baseUrl = "http://10.6.181.69/cfmapi/api/";
        HttpClient Http = new HttpClient();
        protected List<StoreOut> stores;

        protected List<DistributionRegion> regions;

        protected StoreOut storeObj;

        protected override async Task OnInitializedAsync()
        {
            try
            {
                Http.DefaultRequestHeaders.Add("CountryId", "1");
                await GetData(Http);

            }
            catch (Exception ex)
            {

            }
        }

        private async Task GetData(HttpClient Http)
        {
            var response = await Http.GetAsync(baseUrl + "Store");
            var content = await response.Content.ReadAsStringAsync();
            var storeData = JsonConvert.DeserializeObject<ApiOut>(content);
            stores = JsonConvert.DeserializeObject<List<StoreOut>>(Convert.ToString(storeData.Data));
        }
    }
}
