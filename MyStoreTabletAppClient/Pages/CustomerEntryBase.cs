﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using MyStoreTabletAppClient.Data;

namespace MyStoreTabletAppClient.Pages
{
    public class CustomerEntryBase : ComponentBase
    {
        HttpClient Http = new HttpClient();
        protected CustomerMaster[] custs;

        protected CustomerMaster custObj;

        string ids = "0";
        protected bool showAddrow = false;

        protected bool loadFailed;
        string baseUrl = "https://localhost:5001/";

        protected override async Task OnInitializedAsync()
        {
            try
            {
                ids = "0";
                custs = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "/api/CustomerMasters/");
            }
            catch (Exception ex)
            {

            }
        }


        protected void AddNewCustomer()
        {
            ids = "0";
            showAddrow = true;
            custObj = new CustomerMaster();
        }
        // Add New Customer Details Method
        protected async Task AddCustomer()
        {
            try
            {
                if (ids == "0")

                {
                    await Http.SendJsonAsync(HttpMethod.Post, baseUrl + "api/CustomerMasters", custObj);
                    custs = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
                }
                else
                {
                    await Http.SendJsonAsync(HttpMethod.Put, baseUrl + "api/CustomerMasters/" + custObj.CustCd, custObj);
                    custs = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
                }

                showAddrow = false;
            }
            catch (Exception ex)
            {

            }
        }
        // Edit Method
        protected async Task EditCustomer(string CustomerID)
        {
            try
            {
                showAddrow = true;

                ids = "1";
                //try
                //{
                loadFailed = false;
                ids = CustomerID.ToString();
                custObj = await Http.GetJsonAsync<CustomerMaster>(baseUrl + "api/CustomerMasters/" + CustomerID);

                string s = custObj.CustCd;

                showAddrow = true;
            }
            catch (Exception ex)
            {

            }
        }

        // Delte Method
        protected async Task DeleteCustomer(string CustomerID)
        {
            try
            {
                showAddrow = false;

                ids = CustomerID.ToString();
                await Http.DeleteAsync(baseUrl + "api/CustomerMasters/" + CustomerID);

                custs = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
            }
            catch (Exception ex)
            {

            }
        }

    }

}