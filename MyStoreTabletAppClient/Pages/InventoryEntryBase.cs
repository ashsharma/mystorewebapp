﻿using Microsoft.AspNetCore.Components;
using MyStoreTabletAppClient.Data;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Pages
{
    public class InventoryEntryBase : ComponentBase
    {
        HttpClient http = new HttpClient();
        protected Warehouse[] warehouse;

        protected Warehouse warehouseObj;

        string ids = "0";
        protected bool showAddrow = false;

        protected bool loadFailed;
        string baseUrl = "https://localhost:5001/";

        public int count = 0;
        public bool isEditable = false;
        public string isEditDisplay = "block";
        public string isUpdateDisplay = "none";

        public void makeEditable()
        {
            isEditable = true;
            isEditDisplay = "none";
            isUpdateDisplay = "block";
        }
        public void cancelEditable()
        {
            isEditable = false;
            isEditDisplay = "block";
            isUpdateDisplay = "none";
        }
        public void updateInventory()
        {
            isEditable = false;
            isEditDisplay = "block";
            isUpdateDisplay = "none";

            string warehouseOut = Newtonsoft.Json.JsonConvert.SerializeObject(warehouse, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("wwwroot/WarehouseItems.json", warehouseOut);
        }

        public void updateTotalUnits(int itemId)
        {
            var updatedRow = warehouse.FirstOrDefault(x => x.ItemId == itemId);
            updatedRow.TotalUnits = updatedRow.SingleUnits + updatedRow.SlaveUnits + updatedRow.CaseUnits;
            updatedRow.ExpectedUnits = updatedRow.TotalUnits * Convert.ToDecimal(1.1);
            updatedRow.Counted = true;
        }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                ids = "0";
                // fetch mock data for now
                warehouse = await http.GetJsonAsync<Warehouse[]>(baseUrl + "WarehouseItems.json");
                //warehouse = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
            }
            catch (Exception ex)
            {

            }
        }


        //protected void AddNewCustomer()
        //{
        //    ids = "0";
        //    showAddrow = true;
        //    custObj = new CustomerMaster();
        //}
        //// Add New Customer Details Method
        //protected async Task AddCustomer()
        //{
        //    try
        //    {
        //        if (ids == "0")

        //        {
        //            await Http.SendJsonAsync(HttpMethod.Post, baseUrl + "api/CustomerMasters", custObj);
        //            warehouse = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
        //        }
        //        else
        //        {
        //            await Http.SendJsonAsync(HttpMethod.Put, baseUrl + "api/CustomerMasters/" + custObj.CustCd, custObj);
        //            warehouse = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
        //        }

        //        showAddrow = false;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        //// Edit Method
        //protected async Task EditCustomer(string CustomerID)
        //{
        //    try
        //    {
        //        showAddrow = true;

        //        ids = "1";
        //        //try
        //        //{
        //        loadFailed = false;
        //        ids = CustomerID.ToString();
        //        custObj = await Http.GetJsonAsync<CustomerMaster>(baseUrl + "api/CustomerMasters/" + CustomerID);

        //        string s = custObj.CustCd;

        //        showAddrow = true;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        //// Delte Method
        //protected async Task DeleteCustomer(string CustomerID)
        //{
        //    try
        //    {
        //        showAddrow = false;

        //        ids = CustomerID.ToString();
        //        await Http.DeleteAsync(baseUrl + "api/CustomerMasters/" + CustomerID);

        //        warehouse = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

    }

}