﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Data
{
    public class Warehouse
    {
        public int ItemId { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public decimal SingleUnits { get; set; }
        public decimal CaseUnits { get; set; }
        public decimal SlaveUnits { get; set; }
        public decimal TotalUnits { get; set; }
        public decimal ExpectedUnits { get; set; }
        public bool Counted { get; set; }
    }
}