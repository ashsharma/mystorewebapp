﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Data.Models
{
    public class DistributionRegion
    {
        public int id { get; set; }
        public string keyName { get; set; }
        public object keyValue { get; set; }
    }
}
